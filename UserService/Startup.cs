using FluentMigrator.Runner;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using UserService.Auth0;
using UserService.DAL;
using UserService.Migrations;
using UserService.Utils;
using static UserService.Migrations.Database;

namespace UserService {
    public class Startup {

        private readonly IWebHostEnvironment _env;
        private readonly string _dbName = "userDB";

        public Startup(IWebHostEnvironment env) {
            Configuration = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"secrets/appsettings.{env.EnvironmentName}.json", optional: true)
            .SetDevSettingsIfNotInDocker(env)
            .AddEnvironmentVariables()
            .Build();

            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();

            var authApiInfo = new Auth0Info() {
                Token = Configuration.GetSection("Auth0")["Token"],
                ContentType = Configuration.GetSection("Auth0")["ContentType"],
                BaseUrl = Configuration.GetSection("Auth0")["BaseUrl"]
            };
            services.AddSingleton(authApiInfo);

            // Fluent Migrator
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(migrationBuilder =>
                    migrationBuilder.AddSqlServer()
                      .WithGlobalConnectionString(Configuration.GetConnectionString(_dbName))
                      .ScanIn(typeof(InitialCreate).Assembly).For.Migrations()
                )
                .AddLogging(loggerBuilder =>
                    loggerBuilder.AddFluentMigratorConsole()
                );

            if (_env.IsDevelopment()) {
                // TODO: Eventually make a mock for development
                services.AddScoped<IUserApi, Auth0Api>();
            }
            else {
                services.AddScoped<IUserApi, Auth0Api>();
            }

            // Message broker
            services.AddMassTransit(ms => {
                ms.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg => {
                    cfg.UseHealthCheck(provider);
                    cfg.Host(new Uri(Configuration.GetSection("RabbitMQ")["host"]), host => {
                        host.Username(Configuration.GetSection("RabbitMQ")["username"]);
                        host.Password(Configuration.GetSection("RabbitMQ")["password"]);
                    });
                }));
            });

            // Api key for creating users
            services.AddSingleton(new ApiKey(Configuration.GetSection("Auth0")["UserCreatePermissionToken"]));

            services.AddMassTransitHostedService();

            // Database
            services.AddScoped<IUserDal>((_) => new UserDal(Configuration.GetConnectionString(_dbName)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app) {
            if (_env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });

            DoMigrations(app);
        }

        private void DoMigrations(IApplicationBuilder app) {
            EnsureCreated(Configuration.GetConnectionString(_dbName), _dbName);
            // Migrate
            using (var scope = app.ApplicationServices.CreateScope()) {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
        }
    }
}
