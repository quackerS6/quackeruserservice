﻿using FluentMigrator;

namespace UserService.Migrations {
    [Migration(20210330)]
    public class InitialCreate : Migration {

        public override void Up() {
            Create.Table("users")
                .WithColumn("id").AsGuid().PrimaryKey().WithDefaultValue(SystemMethods.NewGuid)
                .WithColumn("auth-id").AsString().NotNullable().Unique()
                .WithColumn("firstname").AsString().NotNullable()
                .WithColumn("lastname").AsString().Nullable()
                .WithColumn("bio").AsString().Nullable()
                .WithColumn("tag").AsString().Nullable()
                .WithColumn("country").AsString().Nullable()
                .WithColumn("city").AsString().Nullable()
                .WithColumn("optional_info_promted").AsBoolean().NotNullable().WithDefaultValue(false);
        }
        public override void Down() {
            Delete.Table("users");
        }
    }
}
