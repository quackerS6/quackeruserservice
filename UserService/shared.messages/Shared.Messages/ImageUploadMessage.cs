﻿using System;

namespace Shared.Messages {
    public class ImageUploadMessage {
        public string Url { get; set; }
        public Guid UserId { get; set; }
    }
}
