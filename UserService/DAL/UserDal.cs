﻿using Dapper;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace UserService.DAL {
    public class UserDal : IUserDal {

        private readonly string _connectionString;
        private readonly string _sqlUpdate = "UPDATE users SET firstname = @Firstname, lastname = @Lastname, bio = @Bio, country = @Country, city = @City, tag = @Tag, optional_info_promted = @OptionalInfoPromted WHERE id = @Id;";
        private readonly string _sqlAdd = "INSERT INTO users ([auth-id], firstname, lastname, bio) OUTPUT INSERTED.id VALUES (@AuthId, @Firstname, @Lastname, @Bio);";
        private readonly string _sqlGet = "SELECT id, [auth-id], firstname, lastname, tag, bio, optional_info_promted as OptionalInfoPromted, city, country  FROM users WHERE id = @Id;";
        private readonly string _sqlGetByTag = "SELECT id, [auth-id], firstname, lastname, tag, bio, optional_info_promted as OptionalInfoPromted, city, country  FROM users WHERE tag = @Tag;";
        private readonly string _sqlGetByAuthId = "SELECT id, [auth-id], firstname, lastname, tag, bio, optional_info_promted as OptionalInfoPromted, city, country  FROM users WHERE [auth-id] = @AuthId;";

        public UserDal(string connectionString) {
            _connectionString = connectionString;
        }

        public async Task<Guid> Add(UserEntity user) {
            using (var connection = new SqlConnection(_connectionString)) {
                return await connection.QuerySingleAsync<Guid>(_sqlAdd, user);
            }
        }

        public async Task<UserEntity> Update(UserEntity updateInfo) {
            using (var connection = new SqlConnection(_connectionString)) {
                UserEntity toUpdate = await connection.QueryFirstAsync<UserEntity>(_sqlGet, new { Id = updateInfo.Id });
                UpdateEntity(updateInfo, toUpdate);
                await connection.ExecuteAsync(_sqlUpdate, toUpdate);
                return toUpdate;
            }
        }

        public async Task<UserEntity> GetById(Guid id) {
            using (var connection = new SqlConnection(_connectionString)) {
                return await connection.QueryFirstAsync<UserEntity>(_sqlGet, new { Id = id });
            }
        }

        public async Task<UserEntity> GetByTag(string tag) {
            using (var connection = new SqlConnection(_connectionString)) {
                return await connection.QueryFirstOrDefaultAsync<UserEntity>(_sqlGetByTag, new { Tag = tag });
            }
        }

        public async Task<UserEntity> GetByAuthId(string authId) {
            using (var connection = new SqlConnection(_connectionString)) {
                return await connection.QueryFirstOrDefaultAsync<UserEntity>(_sqlGetByAuthId, new { AuthId = authId });
            }
        }

        private void UpdateEntity(UserEntity source, UserEntity destination) {
            destination.Tag = string.IsNullOrEmpty(destination.Tag) ? source.Tag : destination.Tag;
            destination.Bio = source.Bio ?? destination.Bio;
            destination.Firstname = source.Firstname ?? destination.Firstname;
            destination.Lastname = source.Lastname ?? destination.Lastname;
            destination.Country = source.Country ?? destination.Country;
            destination.City = source.City ?? destination.City;
            destination.OptionalInfoPromted = source.OptionalInfoPromted;
        }


    }
}
