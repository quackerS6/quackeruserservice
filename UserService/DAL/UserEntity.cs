﻿using System;

namespace UserService.DAL {
    public class UserEntity {
        public Guid Id { get; set; }
        public string AuthId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Bio { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Tag { get; set; }
        public bool OptionalInfoPromted { get; set; } = false;
    }
}
