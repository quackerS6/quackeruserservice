﻿using System;
using System.Threading.Tasks;

namespace UserService.DAL {
    public interface IUserDal {
        Task<Guid> Add(UserEntity user);
        Task<UserEntity> GetById(Guid id);
        Task<UserEntity> GetByAuthId(string authId);
        Task<UserEntity> GetByTag(string tag);
        Task<UserEntity> Update(UserEntity user);
    }
}