﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Shared.Messages;
using System;
using System.Threading.Tasks;
using UserService.Auth0;
using UserService.DAL;
using UserService.Models;
using UserService.Models.Mappers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UserService.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase {

        private readonly IUserDal _db;
        private readonly IPublishEndpoint _broker;
        private readonly UserMapper _mapper;
        private readonly ApiKey _apiKey;

        public UsersController(IUserDal db, IPublishEndpoint broker, ApiKey apiKey) {
            _db = db;
            _broker = broker;
            _mapper = new UserMapper();
            _apiKey = apiKey;
        }

        [HttpGet("{tagOrId}")]
        public async Task<IActionResult> GetByTagOrId([FromRoute] string tagOrId, [FromQuery] bool isTag) {
            IActionResult response;
            UserEntity entity;
            if (isTag) {
                entity = await _db.GetByTag(tagOrId);
            }
            else if (Guid.TryParse(tagOrId, out Guid id)) {
                entity = await _db.GetById(id);
            }
            else {
                return BadRequest(tagOrId);
            }

            if (entity != null) {
                response = Ok(_mapper.ToModel(entity));
            }
            else {
                response = NoContent();
            }
            return response;
        }

        //POST api/<UserController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserCreationModel user) {
            if (!_apiKey.IsValid(user.ApiKey)) {
                return Forbid();
            }

            UserEntity existingUser = await _db.GetByAuthId(user.AuthId);
            if (existingUser != null) {
                return Ok(new { AuthId = existingUser.AuthId, QuackerId = existingUser.Id });
            }

            UserEntity entity = new UserEntity() {
                AuthId = user.AuthId,
                Firstname = string.Empty
            };

            Guid generatedId = await _db.Add(entity);

            await _broker.Publish(new UserCreatedMessage() {
                Id = generatedId
            });

            user.QuackerId = generatedId;

            return Created($"/users/{generatedId}", new { AuthId = user.AuthId, QuackerId = generatedId });
        }

        // PATCH: /api/user/<id>/image
        // TODO: No content if the entity does not exist
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch([FromBody] UserUpdateModel updateModel, [FromHeader] Guid userId) {
            UserEntity entity = _mapper.ToEntity(updateModel);
            entity.Id = userId;
            UserEntity updatedEntity = await _db.Update(entity);
            await _broker.Publish(new UserUpdateMessage() {
                City = updatedEntity.City,
                Country = updatedEntity.Country,
                Firstname = updatedEntity.Firstname,
                Tag = updatedEntity.Tag,
                Id = userId
            });
            return Ok(updatedEntity);
        }
    }
}
