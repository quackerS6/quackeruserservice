﻿namespace UserService.Auth0 {
    public class Auth0Info {
        public string Token { get; set; }
        public string BearerToken {
            get {
                return $"Bearer {Token}";
            }
        }
        public string BaseUrl { get; set; }

        public string SegmentedBaseUrl {
            get {
                return BaseUrl + "{entity}/{id}";
            }
        }

        public string ContentType { get; set; } = "application/json";

    }
}
