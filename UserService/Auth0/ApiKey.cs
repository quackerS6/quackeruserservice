﻿namespace UserService.Auth0 {
    public class ApiKey {
        private readonly string _key;
        public ApiKey(string key) {
            _key = key;
        }

        public bool IsValid(string keyToCheck) {
            return _key == keyToCheck;
        }

    }
}
