﻿using RestSharp;

namespace UserService.Auth0 {
    public class Auth0Api : IUserApi {

        private readonly Auth0Info _apiInfo;
        // Todo: should this be a singleton? || how can i make the rest client static?
        private readonly RestClient _client;

        public Auth0Api(Auth0Info apiInfo) {
            _apiInfo = apiInfo;
            _client = new RestClient(_apiInfo.SegmentedBaseUrl);
            // for static _client.BaseUrl = new Uri(_apiInfo.SegmentedBaseUrl);
        }

        public bool UpdateProfilePicture(string authId, string imgUrl) {
            return PatchUser(authId, new { picture = imgUrl });
        }

        private bool PatchUser(string authId, object jsonBody) {
            var request = new RestRequest(Method.PATCH);
            request.AddUrlSegment("entity", "users")
                   .AddUrlSegment("id", authId)
                   .AddHeader("authorization", _apiInfo.BearerToken)
                   .AddHeader("content-type", _apiInfo.ContentType)
                   .AddJsonBody(jsonBody);
            // Todo: check if I can the request async, also check this for image service
            IRestResponse response = _client.Execute(request);
            // Todo: log unsuccessful responses
            return response.IsSuccessful;
        }
    }
}
