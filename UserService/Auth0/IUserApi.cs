﻿namespace UserService.Auth0 {
    public interface IUserApi {
        bool UpdateProfilePicture(string authId, string imgUrl);
    }
}
