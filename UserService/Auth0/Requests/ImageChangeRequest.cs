﻿namespace UserService.Auth0.Requests {
    public class ImageChangeRequest {
        public string ImgUrl { get; set; }
    }
}
