﻿using System;

namespace UserService.Models {
    public class UserModel {
        public Guid Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Bio { get; set; }
        public bool IsAskedForOptionalInfo { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
