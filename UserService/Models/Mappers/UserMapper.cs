﻿using UserService.DAL;

namespace UserService.Models.Mappers {
    public class UserMapper {
        public UserMapper() {
            // Empty constructor for readability
        }

        public UserModel ToModel(UserEntity source) {
            UserModel destination = new UserModel() {
                Id = source.Id,
                Firstname = source.Firstname,
                Lastname = source.Lastname ?? string.Empty,
                Bio = source.Bio ?? string.Empty,
                City = source.City ?? string.Empty,
                Country = source.Country ?? string.Empty,
                IsAskedForOptionalInfo = source.OptionalInfoPromted
            };
            return destination;
        }

        public UserEntity ToEntity(UserUpdateModel source) {
            UserEntity destination = new UserEntity() {
                Bio = source.Bio,
                Firstname = source.Firstname,
                Lastname = source.Lastname,
                Country = source.Country,
                City = source.City,
                OptionalInfoPromted = source.IsAskedForOptionalInfo,
                Tag = source.Tag
            };
            return destination;
        }

    }
}
