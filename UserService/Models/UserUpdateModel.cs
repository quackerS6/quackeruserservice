﻿namespace UserService.Models {
    public class UserUpdateModel {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Bio { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Tag { get; set; }
        public bool IsAskedForOptionalInfo { get; set; } = false;
    }
}
