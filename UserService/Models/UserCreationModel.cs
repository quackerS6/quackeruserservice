﻿using System;

namespace UserService.Models {
    public class UserCreationModel {
        public string AuthId { get; set; }
        public Guid QuackerId { get; set; }
        public string ApiKey { get; set; }
    }
}
